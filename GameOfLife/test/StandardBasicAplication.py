# Standard Basic Application
    #Create a Basic Application for all Standards

from Tkinter import *
from StandardRandom import *

class StandardBasicApplication:

	def __init__( self, StandardBasic, CanvasMenuGame, GameMode ):

		StandardBasic.geometry( '1335x650' )

		self.CanvasMG = CanvasMenuGame
		self.StdBasic = StandardBasic

		self.Frame = Frame( StandardBasic )
		self.Frame["bg"] = "GRAY"
		self.Frame["bd"] = 1
		self.Frame.pack( side = LEFT )

		self.CanvasStandardBasic = Canvas( self.Frame )
		self.CanvasStandardBasic["bg"] = "GRAY"
		self.CanvasStandardBasic["width"] = 1199
		self.CanvasStandardBasic["heigh"] = 644
		self.CanvasStandardBasic["highlightbackground"] = "WHITE"
		self.CanvasStandardBasic.pack( side = LEFT )

		self.CanvasStandardBasic.create_text(100,40, text='Press Start to Play', fill='BLACK')

		self.ButtonStart = Button( self.Frame )
		self.ButtonStart["bg"] = "WHITE"
		self.ButtonStart["highlightbackground"] = "BLACK"
		self.ButtonStart["bd"] = 0
		self.ButtonStart["width"] = 10
		self.ButtonStart["text"] = "Start"

		if( GameMode == 0 ):
			self.ButtonStart["command"] = self.StdRandomAction
		elif(GameMode == 1 ):
			self.ButtonStart["command"] = self.StdReadyAction
		elif( GameMode == 2 ):
			self.ButtonStart["command"] = self.StdDrawAction

		self.ButtonStart.pack( padx = 15, pady = 20 )

		self.ButtonBack = Button( self.Frame )
		self.ButtonBack["bg"] = "WHITE"
		self.ButtonBack["highlightbackground"] = "BLACK"
		self.ButtonBack["bd"] = 0
		self.ButtonBack["width"] = 10
		self.ButtonBack["text"] = "Back"
		self.ButtonBack["command"] = self.ActionButtonBack
		self.ButtonBack.pack( padx = 15 )

	def ActionButtonBack( self ):

		self.Frame.destroy()
		self.StdBasic.geometry( '350x145' )
		self.CanvasMG.pack()

	def StdRandomAction( self ):

		SRandom = StandardRandom( self.StdBasic, self.CanvasStandardBasic )
		SRandom.RunGameOfLife()

	def StdReadyAction( self ):

		pass

	def StdDrawAction( self ):

		pass
